#!/usr/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

$SCRIPT_DIR/apps/install.sh
$SCRIPT_DIR/apps/setup.sh
cd $SCRIPT_DIR/zsh-config
git submodule init
git submodule update
./install.sh
