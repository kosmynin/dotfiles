#!/usr/bin/bash
set -x

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $HOME/.thunderbird && git commit -am \"$(date +%F-%T)\" && git push
cd $HOME/.zotero/zotero && git commit -am \"$(date +%F-%T)\" && git push
