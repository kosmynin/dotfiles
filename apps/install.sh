#!/usr/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SYSTEM=$(grep -oP '(?<=^ID=).+' /etc/os-release | tr -d '"')
case $SYSTEM in
"ubuntu")
  PI="sudo apt install -y"
  PU="sudo apt update"
  $PU
  ;;
"debian")
  PI="sudo apt install -y"
  PU="sudo apt update"
  $PU
  ;;
"fedora")
  PI="sudo dnf install -y"
  PU="sudo dnf update"
  sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
  sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
  sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
  sudo dnf group update sound-and-video
  sudo dnf group update core
  $PU
  ;;
*)
  PI="sudo pacman -Sy"
  PU="sudo pacman -Syu"
  $PU
  ;;
esac

is_app_installed() {
  type "$1" &>/dev/null
}

# Install flatpak
if ! is_app_installed flatpak ; then
  $PU
  $PI flatpak
fi

# Install flathub repo
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Install flatpaks
flatpak install -y flathub com.github.tchx84.Flatseal com.github.jeromerobert.pdfarranger org.telegram.desktop com.discordapp.Discord im.riot.Riot org.zotero.Zotero com.moonlight_stream.Moonlight org.darktable.Darktable com.mattjakeman.ExtensionManager com.nextcloud.desktopclient.nextcloud

# Install native apps
if ! is_app_installed zsh; then
  $PI zsh util-linux-user
  chsh -s $(which zsh)
fi

if ! is_app_installed exa; then
  $PI exa
fi

if ! is_app_installed bat; then
  $PI bat
fi

if ! is_app_installed wormhole; then
  sudo pip install magic-wormhole
fi

if ! is_app_installed vim; then
  $PI vim
fi

if ! is_app_installed variety; then
  $PI variety
fi

if ! is_app_installed okular; then
  $PI okular
fi

if ! is_app_installed thunderbird; then
  $PI thunderbird
fi

if ! is_app_installed code; then
  case $SYSTEM in
  "ubuntu")
    sudo apt install -y software-properties-common apt-transport-https
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
    sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
    sudo apt update
    sudo apt install -y code
    ;;
  "debian")
    sudo apt install -y software-properties-common apt-transport-https
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
    sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
    sudo apt update
    sudo apt install -y code
    ;;
  "fedora")
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
    dnf check-update
    sudo dnf install -y code
    ;;
  *)

    ;;
  esac

  bash -c "$(wget -q -O - https://linux.kite.com/dls/linux/current)"
  systemctl --user start kite-autostart
fi

if ! is_app_installed brave; then
  case $SYSTEM in
  "ubuntu")
    sudo apt install curl
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
    sudo apt update
    sudo apt install brave-browser
    ;;
  "debian")
    sudo apt install curl
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
    sudo apt update
    sudo apt install brave-browser
    ;;
  "fedora")
    sudo dnf install -y dnf-plugins-core
    sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/brave-browser.repo
    sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
    sudo dnf install -y brave-browser
    ;;
  *)

    ;;
  esac
fi

if ! is_app_installed zoom; then
  case $SYSTEM in
  "ubuntu")
    wget https://zoom.us/client/latest/zoom_amd64.deb
    sudo apt install -y ./zoom_amd64.deb
    rm zoom_x86_64.deb
    ;;
  "debian")
    wget https://zoom.us/client/latest/zoom_amd64.deb
    sudo apt install -y ./zoom_amd64.deb
    rm zoom_x86_64.deb
    ;;
  "fedora")
    wget https://zoom.us/client/latest/zoom_x86_64.rpm
    sudo dnf localinstall -y zoom_x86_64.rpm
    rm zoom_x86_64.rpm
    ;;
  *)

    ;;
  esac
fi

curl -s https://install.zerotier.com | sudo bash

# Install NerdFont
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/SourceCodePro.zip
unzip SourceCodePro.zip -d $HOME/.local/share/fonts/
rm SourceCodePro.zip
