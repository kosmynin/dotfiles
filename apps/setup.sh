#!/usr/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

mkdir $HOME/.config/variety
ln -sf $SCRIPT_DIR/variety.conf $HOME/.config/variety/variety.conf

# Get Secrets
$SCRIPT_DIR/bw config server https://bitwarden.kosmynin.de
$SCRIPT_DIR/bw login
SESSION=$($SCRIPT_DIR/bw unlock --raw)

mkdir $HOME/.ssh
$SCRIPT_DIR/bw get attachment ssh-key --itemid 2bcfae00-a22f-406a-bc08-0abd8c5e6931 --output $HOME/.ssh/id --session "$SESSION"
$SCRIPT_DIR/bw get attachment ssh-config --itemid 2bcfae00-a22f-406a-bc08-0abd8c5e6931 --output $HOME/.ssh/config --session "$SESSION"

git clone ssh://git@gitlab.com/kosmynin/dotfiles.thunderbird.git $HOME/.thunderbird
git clone ssh://git@gitlab.com/kosmynin/dotfiles.zotero.git $HOME/.zotero/zotero

gsettings set org.gnome.SessionManager auto-save-session true
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
gsettings set org.gnome.desktop.wm.preferences button-layout "appmenu:minimize,close"
dconf write /org/gnome/desktop/wm/interface/clock-show-weekday true
